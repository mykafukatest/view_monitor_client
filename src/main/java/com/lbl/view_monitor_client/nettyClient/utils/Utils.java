package com.lbl.view_monitor_client.nettyClient.utils;


public class Utils {
	public static byte[] int2Bytes(int i) {
		byte []by=new byte[4];
		by[0]=(byte)i;
		by[1]=(byte)(i>>8);
		by[2]=(byte)(i>>16);
		by[3]=(byte)(i>>24);
		return by;
	}
	public static int bytes2int(byte[] by) {
		return (by[0]&0xFF)
				|((by[1]&0xFF)<<8)
				|((by[2]&0xFF)<<16)
				|((by[3]&0xFF)<<24);
	}
	public static byte[] long2Bytes(long i) {
		byte []by=new byte[8];
		by[0]=(byte)i;
		by[1]=(byte)(i>>8);
		by[2]=(byte)(i>>16);
		by[3]=(byte)(i>>24);
		by[4]=(byte)(i>>32);
		by[5]=(byte)(i>>40);
		by[6]=(byte)(i>>48);
		by[7]=(byte)(i>>56);
		return by;
	}
	public static long bytes2Long2(byte[] by) {
		return (long)(by[0]&0xFF)
				|((long)(by[1]&0xFF)<<8)
				|((long)(by[2]&0xFF)<<16)
				|((long)(by[3]&0xFF)<<24)
				|((long)(by[4]&0xFF)<<32)
				|((long)(by[5]&0xFF)<<40)
				|((long)(by[6]&0xFF)<<48)
				|((long)(by[7]&0xFF)<<56);
	}
}
