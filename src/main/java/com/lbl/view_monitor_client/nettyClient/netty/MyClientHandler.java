package com.lbl.view_monitor_client.nettyClient.netty;

import com.lbl.view_monitor_client.nettyClient.utils.RobotServerUtils;
import com.lbl.view_monitor_client.nettyClient.view.MyMyClinetStudnet;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class MyClientHandler extends SimpleChannelInboundHandler<MessageProtocol> {
    private MyMyClinetStudnet myMyClinetStudnet;
    private HashMap<Integer, UnitFrame> map=new HashMap<>();
    private long currentId=0;

    public MyMyClinetStudnet getMyMyClinetStudnet() {
        return myMyClinetStudnet;
    }

    public void setMyMyClinetStudnet(MyMyClinetStudnet myMyClinetStudnet) {
        this.myMyClinetStudnet = myMyClinetStudnet;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info("client active ...");
        myMyClinetStudnet.setChannelHandlerContext(ctx);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageProtocol msg) throws Exception {
        try {
            byte[] zipReceiveImage = RobotServerUtils.zipReceiveImage(msg.getContent());
            UnitFrame unitFrame = RobotServerUtils.conversionUnitFrame(zipReceiveImage,zipReceiveImage.length);
            if(unitFrame.getTimeId().equals(currentId)) {
                map.put(unitFrame.getFrameNo(), unitFrame);
            }else if(unitFrame.getTimeId()>currentId){
                currentId=unitFrame.getTimeId();
                map.clear();
                map.put(unitFrame.getFrameNo(), unitFrame);
            }

            myMyClinetStudnet.assembledPicture(map);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.info("异常消息 {}" , cause.getMessage());
        ctx.close();
    }
}