package com.lbl.view_monitor_client.nettyClient.netty;

import com.lbl.view_monitor_client.nettyClient.view.MyMyClinetStudnet;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;


public class MyClientInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        MyClientHandler handler = new MyClientHandler();
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new MyMessageEncoder()); //加入编码器
        pipeline.addLast(new MyMessageDecoder()); //加入解码器
        handler.setMyMyClinetStudnet(new MyMyClinetStudnet());
        pipeline.addLast(handler);
    }
}