package com.lbl.view_monitor_client.nettyClient.netty;

public class UnitFrame {
	/**
	 * 8个字节的时间戳
	 * 1个字节的报文总个数
	 * 1个字节的报文编号
	 * 4个字节的报文长度
	 * dataArray[]数据的长度
	 */
	private Long timeId;
	
	private Integer count;
	
	private Integer frameNo;
	
	private Integer datalen;
	
	private byte[] dataArray;

	public Long getTimeId() {
		return timeId;
	}

	public void setTimeId(Long timeId) {
		this.timeId = timeId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(Integer frameNo) {
		this.frameNo = frameNo;
	}

	public Integer getDatalen() {
		return datalen;
	}

	public void setDatalen(Integer datalen) {
		this.datalen = datalen;
	}

	public byte[] getDataArray() {
		return dataArray;
	}

	public void setDataArray(byte[] dataArray) {
		this.dataArray = dataArray;
	}
	
	
	
	
}
