package com.lbl.view_monitor_client.nettyClient.netty;

import lombok.Data;

@Data
public class MessageProtocol {
    private int len; //关键
    private byte[] content;
}