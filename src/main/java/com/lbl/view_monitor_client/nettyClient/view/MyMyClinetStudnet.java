package com.lbl.view_monitor_client.nettyClient.view;

import com.lbl.view_monitor_client.nettyClient.main.Constant;
import com.lbl.view_monitor_client.nettyClient.netty.MessageProtocol;
import com.lbl.view_monitor_client.nettyClient.netty.UnitFrame;
import com.lbl.view_monitor_client.nettyClient.utils.Utils;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * 视图类
 */
@Slf4j
public class MyMyClinetStudnet extends JFrame{
	private ChannelHandlerContext channelHandlerContext;
	/**
	 * 60字节
	 */
	private JLabel label;

	public void setChannelHandlerContext(ChannelHandlerContext channelHandlerContext) {
		this.channelHandlerContext = channelHandlerContext;
	}

	public MyMyClinetStudnet() {//这里随便找个图片改一下路径
		ImageIcon image=new ImageIcon("C:\\Users\\admin\\Pictures\\221545.jpg");
		label=new JLabel(image);
		label.setBounds(0, 0,1920, 1080);
		this.add(label);
		this.setTitle("录屏");
		this.setLayout(null);
		this.setSize(1920, 1080);
		this.setLocation(100,10);
		//设置去掉边框
		this.setUndecorated(true);
		this.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {

			}

			@Override
			public void mouseMoved(MouseEvent e) {
				byte[] data =new byte[9];
				data[0]=Constant.MOUSEMOVE;
				channelHandlerContext.writeAndFlush(buildXY(data,e.getX()+370,e.getY()-30));
			}
		});
		this.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				byte[] data =new byte[9];
				data[0]=Constant.MOUSECLICK;
				System.out.println(e.getX()+":"+e.getY());
				channelHandlerContext.writeAndFlush(buildXY(data,e.getX()+370,e.getY()-30));
			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}
		});
		this.setLocation(0, 0);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);
	}
	public MessageProtocol buildXY(byte[] data,int x,int y){

		byte[] xArr = Utils.int2Bytes(x);
		byte[] yArr = Utils.int2Bytes(y);
		System.arraycopy(xArr,0,data,1,4);
		System.arraycopy(yArr,0,data,5,4);
		MessageProtocol protocol = new MessageProtocol();
		protocol.setLen(data.length);
		protocol.setContent(data);
		return protocol;
	}
	public void assembledPicture(HashMap<Integer, UnitFrame> map) throws IOException {
		ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();
		Integer count = map.values().iterator().next().getCount();
		if(map.size()==count) {//转换图片
			for (int i = 0; i < count; i++) {
				UnitFrame frame = map.get(i);
				byte[] array = frame.getDataArray();
				try {
					arrayOutputStream.write(array);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			updateImage(arrayOutputStream.toByteArray());
		}
	}

	public void updateImage(byte[] images) throws IOException {
		ImageIcon icon=new ImageIcon(images);
		//将图片保存到本地
//		saveLocalImage(Constant.LOCALSAVEIMAGEPATH,images);
		label.setIcon(icon);
	}
	private void saveLocalImage(String pathPre,byte[] images) throws IOException {
		FileOutputStream fileOutputStream=new FileOutputStream(pathPre+System.currentTimeMillis()+".jpg");
		fileOutputStream.write(images);
		fileOutputStream.close();
	}
}
